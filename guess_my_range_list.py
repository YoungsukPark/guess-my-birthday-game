# Instructions for human
# Guide by instructor

from random import randint
print("Think of a number between 1 and 50")

# Create an empty list to hold guesses
guesses = []

low = 1
high = 50

result = ""

for guess_number in range(5):
    # Generate a random number between 1 and 50.
    guess = randint(low, high)

    # print("Guess number: ", guess_number)
    # print ("guess: ", guess)

    # Check if response is equal, higher or lower,
    response = input(f"Is your number equal, higher, or lower than {guess}.")

    if response == "equal":
        result = "equal"
        print("I guessed it!")
        break
    elif response == "higher":
        result = "too low"
        # scenario for higher
        print("My guess was too low.")
        low = guess + 1
        # [result, guess]
        # my_list.append(3)
        guesses.append([guess, result])
        # 25
        # [[30, "too high"], [20, "too low"]]
    elif response == "lower":
        result = "too high"
        print("My guess was too high.")
        high = guess - 1
        guesses.append([guess, result])

# I made 4 guesses
print(f"I made {len(guesses)} guesses")

# iterate over guesses using a for loop
for item in guesses:
    print(item[0], item[1])
    # If you want to put: I guessed 25 which was too low
    # item = guesses[0]
    # item = guesses[1]
    # ...
    # item = guesses[4]


if result == "equal":
    print("We won the game together!")
elif result == "too high":
    print("My guess was too high.")
elif result == "too low":
    print("My guess was too low.")

# What would this do
# my_list = [2, 5, 7]
# for num in my_list:
#     my_list.append(num + 1)
#
