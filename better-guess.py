
# Asks your name with the prompt "Hi! What is your name?"

# Tries to guess your birth month and year

# with a prompt formatted like Guess

# «guess number» : «name» were you born in «m» / «yyyy» ?

# then prompts with "yes or no?"

# Allow the computer to guess 5times


from random import randint

name = input("Hi! What's your name? ")

# A empty list to hold the guesses

for guess_num in range(1, 8):
    month = randint(1, 12)
    days = randint(1, 31)
    year = randint(1924, 2004)

    print("Guess", guess_num, ":", name,
          ", were you born in", month, "/", days, "/", year, "?")

    answer = input("Enter 'Y' for Yes and 'N' for No: ")

    if answer == "Y":
        print("I knew it!")
        exit(print("Thank you for playing!"))
    elif guess_num == 7:
        print("That's unfortunate. ")
        exit(print("Good bye."))
    else:
        print("Drat! Let me try that again..")
