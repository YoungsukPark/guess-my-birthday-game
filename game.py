name = input("What is your name? ")

for guess_birth in range(0, 4):

    from random import randint

    months = randint(1, 12)
    years = randint(1924, 2004)

    # Asks your name with the prompt "Hi! What is your name?"
    # Tries to guess your birth month and year
    # with a prompt formatted like Guess
    # «guess number» : «name» were you born in «m» / «yyyy» ?
    # then prompts with "yes or no?"
    # Allow the computer to guess 5times

    print(name, "were you born in",
          months, "m", years, "y", "?")

    user_input = input("Yes or No? ")

    if user_input == "Yes":
        print("I knew it!")
        exit(print("Thank you for playing!"))
    else:
        print("Drat! Lemme try again!")

for guess_birth in range(4, 5):

    from random import randint

    months = randint(1, 12)
    years = randint(1924, 2004)
    print(name, "were you born in", months, "m", years, "y", "?")
    user_input = input("Yes or No? ")

    if user_input == "Yes":
        print("I knew it!")
        exit(print("Thank you for playing!"))
    else:
        print("I have other things to do. Good bye.")
        exit()
